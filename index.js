var fs = require('fs');

var inputFile = "input.txt",
    outputFile = "output.txt";

// Make sure we got a filename on the command line.
if (process.argv.length == 4) {
    inputFile = process.argv[2];
    outputFile = process.argv[3];
}

// string replace with array to string
String.prototype.replaceArray = function(find, replace) {
    var replaceString = this;
    for (var i = 0; i < find.length; i++) {
        while(true) {
            var oldString = replaceString;
            replaceString = replaceString.replace(find[i], replace);
            if (oldString == replaceString) break;
        }
    }
    return replaceString;
};

// prepare function
String.prototype.prepareArray = function() {
    return this.replaceArray(['[',']','\'',' '], '').split(',');
}

// compare two string
String.prototype.compareTwo = function(comparedString) {
    if (this.length != comparedString.length) return false;
    let countThis = {}, countComp = {};
    for (let key = 0; key < this.length; key++) {
        let pt = this[key], pc = comparedString[key];
        if (Object.keys(countThis).indexOf(pt) >= 0) countThis[pt] ++;
        else countThis[pt] = 1;
        if (Object.keys(countComp).indexOf(pt) >= 0) countComp[pt] ++;
        else countComp[pt] = 1;
    }
    return JSON.stringify(countThis) === JSON.stringify(countComp);
}

// first problem
function solveFirst(words) {
    let results = [];
    for (let kw in words) {
        let w = words[kw];
        let flag = false;
        for (let kr in results) {
            let r = results[kr];
            if (!flag && w.compareTwo(r[0])) {
                r.push(w);
                flag = true;
            } 
        }
        if (!flag) {
            results.push([w]);
        }
    }
    return results;
}

function recursive(str, pos, flag) {
    let res = "", len = 0;
    for (p = pos; p < str.length; ) {
        if (str[p] == "(") {
            temp = recursive(str, p+1, !flag);
            if (flag) res = temp[0] + res;
            else res = res + temp[0];
            len = len + temp[1] + 1;
        }
        else if (str[p] == ")") {
            len++;
            return [res, len];
        }
        else {
            if (flag) res = str[p] + res;
            else res = res + str[p];
            len++;
        }
        p = pos + len;
    }
    return [res, len];
}

// second problem
function solveSecond(words) {
    let results = [];
    for (let kw in words) {
        results[kw] = "";
        let w = words[kw];
        results[kw] = recursive(w, 0, false)[0];
    }
    return results;
}

// Read the file and print its contents.
fs.readFile(inputFile, 'utf8', function(err, data) {
    if (err) throw err;
    console.log('read : ' + inputFile);
    
    // split lines to 2 problems
    var arrayData = data.split('\n');
    var firstData = arrayData[0].prepareArray();
    var secondData = arrayData[1].prepareArray();
    var result = "1.\n" + solveFirst(firstData).join('\n') + "\n2.\n" + solveSecond(secondData).join('\n');

    fs.writeFile(outputFile, result, function(err, res) {
        if (err) console.log("error" + err);
        else console.log("write : " + outputFile + "\nsuccess");
    });
});